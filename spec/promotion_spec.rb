# frozen_string_literal: true

require 'spec_helper'
require 'promotion'

RSpec.describe Promotion do
  it { is_expected.to respond_to(:type) }

  describe '#initialize' do
    let(:type) { 'value' }
    let(:threshold) { 15 }
    subject { described_class.new(type: type, threshold: threshold) }

    it 'correctly assigns the type of promotion' do
      expect(subject.type).to eq('value')
    end

    it 'assigns threshold for the promotion' do
      expect(subject.threshold).to eq(15)
    end

    it 'assigns a default of "value" when no type given' do
      expect(described_class.new.type).to eq('value')
    end

    it 'assigns a default threshold of 0 when non given' do
      expect(described_class.new.threshold).to eq(0)
    end
  end
end
