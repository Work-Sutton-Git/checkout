# frozen_string_literal: true

require 'spec_helper'
require 'item'

RSpec.describe Item do # rubocop:disable Metrics/BlockLength
  it { is_expected.to respond_to(:code) }
  it { is_expected.to respond_to(:name) }
  it { is_expected.to respond_to(:price) }

  describe '#initialize' do # rubocop:disable Metrics/BlockLength
    let(:code) { '001' }
    let(:name) { 'Lavender heart' }
    let(:price) { 925 }

    context 'with invalid attributes' do
      item = Item.new(code: nil, name: nil, price: nil)

      it 'is not valid when all attributes are nil' do
        expect(item).to_not be_valid
      end

      it 'is not valid when :code is nil' do
        item = Item.new(code: nil, name: name, price: price)
        expect(item).to_not be_valid
      end

      it 'is not valid when :name is nil' do
        item = Item.new(code: code, name: nil, price: price)
        expect(item).to_not be_valid
      end

      it 'is not valid when :price is nil' do
        item = Item.new(code: code, name: name, price: nil)
        expect(item).to_not be_valid
      end
    end

    context 'with valid attributes' do
      subject { Item.new(code: code, name: name, price: price) }

      it 'has a code of "001"' do
        expect(subject.code).to eq('001')
      end

      it 'has a name of "Lavender heart"' do
        expect(subject.name).to eq('Lavender heart')
      end

      it 'has a price of "925" pence (£9.25)' do
        expect(subject.price).to eq(925)
      end
    end
  end
end
