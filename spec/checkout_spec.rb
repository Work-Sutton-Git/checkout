# frozen_string_literal: true

require 'spec_helper'
require 'checkout'
require 'item'
require 'promotion'
require 'item_promotion'
require 'total_value_promotion'

RSpec.describe Checkout do # rubocop:disable Metrics/BlockLength
  subject { described_class.new([]) }
  it { is_expected.to respond_to(:items) }
  it { is_expected.to respond_to(:promotional_rules) }
  it { is_expected.to respond_to(:total) }

  describe '#initialize' do
    context 'with zero arguments' do
      it 'raises ArgumentError' do
        expect { described_class.new }.to raise_error(ArgumentError, 'wrong number of arguments (given 0, expected 1)')
      end
    end

    context 'with promotional_rules' do
      it 'doesn\t raise ArgumentError' do
        expect { described_class.new(nil) }.not_to raise_error(ArgumentError)
      end

      it 'assigns promotional_rules to instance variable' do
        expect(subject.promotional_rules).to be_empty
      end

      describe '@items Array' do
        it 'creates the "items" instance variable of type "Array"' do
          expect(subject.items).to be_an(Array)
        end

        it 'is empty' do
          expect(subject.items).to be_empty
        end
      end

      it 'sets total to zero (cents)' do
        expect(subject.total).to eq 0
      end
    end
  end

  describe '#scan' do
    item_001 = Item.new(code: '001', name: 'Lavender heart', price: 925)

    it 'increases Array size by 1' do
      expect { subject.scan(item_001) }.to change { subject.items.count }.from(0).to(1)
    end

    it 'adds item_001 to @items array' do
      subject.scan(item_001)
      expect(subject.items).not_to be_empty
      expect(subject.items).to include(item_001)
    end
  end

  describe '#total' do # rubocop:disable Metrics/BlockLength
    let(:item_001) { Item.new(code: '001', name: 'Lavender heart', price: 925) }
    let(:item_002) { Item.new(code: '002', name: 'Personalised cufflinks', price: 4500) }
    let(:item_003) { Item.new(code: '003', name: 'Kids T-shirt', price: 1995) }

    context 'when no promotions running' do
      it 'has total of zero when @items is empty' do
        expect(subject.total).to eq 0
      end

      it 'has total of 1995' do
        subject.scan(item_003)

        expect(subject.total).to eq 1995
      end

      it 'has total of 7420' do
        subject.scan(item_001)
        subject.scan(item_002)
        subject.scan(item_003)

        expect(subject.total).to eq 7420
      end
    end

    context 'when promotion(s) is/are running' do # rubocop:disable Metrics/BlockLength
      let(:total_value_promotion) { TotalValuePromotion.new(threshold: 6000, discount: 10, type: 'percent') }
      let(:multi_item_promotions) { ItemPromotion.new(threshold: 2, price: 850, code: '001') }
      let(:promotional_rules) { [total_value_promotion, multi_item_promotions] }
      subject { described_class.new(promotional_rules) }

      describe 'total value promotion' do
        before do
          subject.scan(item_001)
          subject.scan(item_002)
          subject.scan(item_003)
        end

        it 'has total of £66.78 (6678 cents)' do
          expect(subject.total).to eq(6678)
        end
      end

      describe 'multi-item promotion' do
        before do
          2.times { subject.scan(item_001) }
        end

        it 'has total of £17.00 (1700 cents) when two or more of item_001 (Lavender heart)' do
          expect(subject.total).to eq(1700)
        end

        it 'has total of £36.95 (3695 cents)' do
          subject.scan(item_003)

          expect(subject.total).to eq(3695)
        end
      end

      describe 'multi-item & total value combined discount' do
        it 'has "total" of £73.76 (7376 cents) for items "001", "002", "001" & "003"' do
          subject.scan(item_001)
          subject.scan(item_002)
          subject.scan(item_001)
          subject.scan(item_003)

          expect(subject.total).to eq(7376)
        end
      end
    end
  end
end
