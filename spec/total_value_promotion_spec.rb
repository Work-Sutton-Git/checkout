# frozen_string_literal: true

require 'spec_helper'
require 'promotion'
require 'total_value_promotion'

RSpec.describe TotalValuePromotion do
  it { is_expected.to respond_to(:type) }
  it { is_expected.to respond_to(:discount) }
  it { is_expected.to respond_to(:threshold) }

  describe '#intitialize' do
    context 'with invalid "type" argument' do
      it 'raises ArgumentError' do
        expect { described_class.new(threshold: 6000, discount: 10, type: 'cat') }.to raise_error(ArgumentError)
      end
    end

    context 'with valid attributes' do
      subject { described_class.new(threshold: 6000, discount: 10, type: 'value') }

      it 'doesn\'t raise ArgumentError' do
        expect { subject }.not_to raise_error
      end

      it 'assigns discount to instance variable' do
        expect(subject.discount).to eq(10)
      end

      it 'inherits threshold from parent class instance variable' do
        expect(subject.threshold).to eq(6000)
      end
    end
  end
end
