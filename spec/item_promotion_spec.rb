# frozen_string_literal: true

require 'spec_helper'
require 'promotion'
require 'item_promotion'

RSpec.describe ItemPromotion do
  it { is_expected.to respond_to(:type) }
  it { is_expected.to respond_to(:threshold) }
  it { is_expected.to respond_to(:code) }
  it { is_expected.to respond_to(:price) }

  describe '#intitialize' do
    context 'with valid attributes' do
      subject { described_class.new(threshold: 2, price: 850, code: '001') }

      it 'doesn\'t raise ArguementError' do
        expect { subject }.not_to raise_error
      end

      it 'inherits type from parent class instance variable' do
        expect(subject.type).to eq('item')
      end

      it 'inherits threshold from parent class instance variable' do
        expect(subject.threshold).to eq(2)
      end

      it 'assigns code to instance variable' do
        expect(subject.code).to eq('001')
      end

      it 'assigns price to instance variable' do
        expect(subject.price).to eq(850)
      end
    end
  end
end
