# Design Decisions

- build using BDD/TDD (where practical)
- implement an Item class (hindsight, probably should have called this Product)
- implement a Checkout class to be used to scan and total Items purchased
- implement a Promotion (base) class
- implement a TotalValuePromotion class - this can be of type 'percent' or 'value'
- implement a ItemPromotion class
- create a yml file to hold the Promotions
- create a 'runner.rb' script file to run examples


# Problems/Issues/Design Changes

- on introspection I probably should have built this outside-in from the Checkout class down, as opposed to from the Item back to the Checkout. Again this was driven by how I wanted to test/design as part of BDD/TDD.
- I should possible have used Struts for the Promotions
- I should possibly have added methods to calculate the totals in the Promotion classes

# TODO
- could build a TotalPercentagePromotion class
- possibly some refactoring


# Tech Checkout Test

Here is a solution to a problem I was given.

## How to execute (with examples given)

`ruby runner.rb`
`rspec`

## How to excute manually

```ruby
irb
require './lib/checkout'
require './lib/item'
require './lib/promotion'
require './lib/total_value_promotion'
require './lib/item_promotion'

promotion_rules = []
TotalValuePromotion.new(threshold: 6000, discount: 10)
ItemPromotion.new(threshold: 2, price: 850, code: '001')
promotion_rules << TotalValuePromotion
promotion_rules << ItemPromotion

co = Checkout.new(promotion_rules)

item_001 = Item.new(code: '001', name: 'Lavender heart', price: 925)

co.scan(item_001)
co.total
```

### Assumptions:
* you have ruby 2.6.6 installed
* you have RSpec 3 installed
* run from within a terminal session
* you have unzipped the file
