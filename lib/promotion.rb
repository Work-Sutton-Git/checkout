# frozen_string_literal: true

# Promotions for the online marketplace
class Promotion
  attr_reader :type, :threshold

  def initialize(type: 'value', threshold: 0)
    @type = type
    @threshold = threshold
  end
end
