# frozen_string_literal: true

# Items are the products in our online marketplace
class Item
  attr_reader :code, :name
  attr_accessor :price

  def initialize(code: nil, name: nil, price: nil)
    @code = code
    @name = name
    @price = price
    @original_price = price
  end

  def valid?
    return false unless code.is_a?(String) && name.is_a?(String) && price.is_a?(Integer)

    true
  end
end
