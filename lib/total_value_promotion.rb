# frozen_string_literal: true

# Promotions for total spend
class TotalValuePromotion < Promotion
  attr_reader :discount

  def initialize(threshold: 0, discount: 0, type: 'percent')
    raise ArgumentError, 'type: Valid Types (\'percent\' || \'value\')' unless %w[percent value].include?(type)

    super(type: type, threshold: threshold)
    @discount = discount
  end
end
