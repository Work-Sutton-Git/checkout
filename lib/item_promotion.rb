# frozen_string_literal: true

# Promotions for multi-buy items
class ItemPromotion < Promotion
  attr_reader :code, :price

  def initialize(threshold: 0, price: 0, code: '0')
    super(type: 'item', threshold: threshold)
    @price = price
    @code = code
  end
end
