# frozen_string_literal: true

# Checkout (Till) which scans items for purchase and calculates the total price
class Checkout
  attr_reader :items, :promotional_rules

  def initialize(promotional_rules)
    @promotional_rules = promotional_rules
    @items = []
    @total = 0 # cents
  end

  def scan(item)
    @items << item
  end

  def total
    calculate_items_discount if item_promotions?
    return total_value_promotion(running_total) if total_value_promotion?

    running_total
  end

  private

  def running_total
    @items.inject(0) { |total, item| total + item.price }
  end

  def total_value_promotion?
    return false if Array(@promotional_rules).empty?

    @promotional_rules.any? { |rules| rules.class == TotalValuePromotion }
  end

  def total_value_promotion(total_before_discount)
    promotion = @promotional_rules.find { |rules| rules.class == TotalValuePromotion }

    if total_before_discount > promotion.threshold
      calculate_total_value_discount(total_before_discount, promotion.discount)
    else
      total_before_discount
    end
  end

  def calculate_total_value_discount(total, discount)
    total - (total * discount / 100)
  end

  def item_promotions?
    return false if Array(@promotional_rules).empty?

    @promotional_rules.any? { |rules| rules.class == ItemPromotion }
  end

  def calculate_items_discount
    process_item_codes(group_items_by_code.keys)
  end

  def process_item_codes(item_codes)
    item_codes.each { |item_code| process_item_promotion(promotion_for_item_code(item_code)) }
  end

  def process_item_promotion(item_promotion)
    return if item_promotion.nil?

    item_collection = @items.find_all { |item| item.code == item_promotion.code }
    apply_item_discount(item_collection, item_promotion) if item_collection.size >= item_promotion.threshold
  end

  def promotion_for_item_code(item_code)
    @promotional_rules.find { |rules| rules.class == ItemPromotion && rules.code == item_code }
  end

  def apply_item_discount(item_collection, item_promotion)
    item_collection.each { |item| item.price = item_promotion.price }
  end

  def group_items_by_code
    @items.group_by(&:code)
  end
end
