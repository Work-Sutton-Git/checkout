require 'yaml'
require './lib/checkout'
require './lib/item'
require './lib/promotion'
require './lib/total_value_promotion'
require './lib/item_promotion'
require 'pry'

# Build the Items (Should have called them Products)
# Product code | Name | Price
#-------------------------------------------
#001 | Lavender heart | £9.25
item_001 = Item.new(code: '001', name: 'Lavender heart', price: 925)

#002 | Personalised cufflinks | £45.00
item_002 = Item.new(code: '002', name: 'Personalised cufflinks', price: 4500)

#003 | Kids T-shirt | £19.95
item_003 = Item.new(code: '003', name: 'Kids T-shirt', price: 1995)


# Parse the data file holding promotions
parsed = begin
  YAML.safe_load(File.open("./data/promotions.yml"))
rescue ArgumentError => e
  puts "Could not parse YAML: #{e.message}"
end

@promotion_rules = []
p "\n\n ------------------------------------"
p parsed
p "--------------------------------\n\n"
parsed.each do |promotion|
  @promotion_rules << TotalValuePromotion.new(threshold: promotion['threshold'], discount: promotion['discount']) if promotion['type'] == 'percent'
  @promotion_rules << ItemPromotion.new(threshold: promotion['threshold'], price: promotion['price'], code: promotion['code']) if promotion['type'] == 'item'
  @promotion_rules
end

# ============================
# Basket: 001,002,003
# Total price expected: £74.20
puts "\n\n==================\nBasket: 001,002,003\nNo Promotions\nTotal price expected: £74.20"

# Create an instance of Checkout
co_no_rules = Checkout.new(nil)

co_no_rules.scan(item_001) if item_001.valid?
co_no_rules.scan(item_002) if item_002.valid?
co_no_rules.scan(item_003) if item_003.valid?

p co_no_rules.items

p co_no_rules.total

# ============================
# Basket #1: 001,002,003
# Total price expected: £66.78
puts "\n\n==================\nBasket: 001,002,003\nTotal Value Promotion\nTotal price expected: £66.78"

co_with_rules_1 = Checkout.new(@promotion_rules)

co_with_rules_1.scan(item_001) if item_001.valid?
co_with_rules_1.scan(item_002) if item_002.valid?
co_with_rules_1.scan(item_003) if item_003.valid?

p co_with_rules_1.items

p co_with_rules_1.total

# ============================
# Basket: 001,003,001
# Total price expected: £36.95
puts "\n\n==================\nBasket: 001,003,001\nMulti-Item Promotion\nTotal price expected: £36.95"

co_with_rules_2 = Checkout.new(@promotion_rules)

co_with_rules_2.scan(item_001) if item_001.valid?
co_with_rules_2.scan(item_003) if item_003.valid?
co_with_rules_2.scan(item_001) if item_001.valid?

p co_with_rules_2.items

p co_with_rules_2.total


# ============================
# Basket: 001,002,001,003
# Total price expected: £73.76
puts "\n\n==================\nBasket: 001,002,001,003\nTotal Value & Multi-Item Promotions\nTotal price expected: £73.76"
co_with_rules_3 = Checkout.new(@promotion_rules)

co_with_rules_3.scan(item_001) if item_001.valid?
co_with_rules_3.scan(item_002) if item_002.valid?
co_with_rules_3.scan(item_001) if item_001.valid?
co_with_rules_3.scan(item_003) if item_003.valid?

p co_with_rules_3.items

p co_with_rules_3.total
